## Interface: 60000
## Title: |cffC495DDBuffs Notice|r
## Version: 2.00
## Notes: Notice the player if one of your buffs or weapons enchant is missing.
## Author: Tukz, Azilroka
## OptionalDeps: ElvUI, Tukui, DuffedUI, AsphyxiaUI
## X-Tukui-ProjectID: 108
## X-Tukui-ProjectFolders: BuffsNotice

Core.lua